/*
 */
package mamachine.client;

import JSONMa.JSONMa;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utility.MaLog;

/**
 *
 * @author maria
 */
public class ClientConf {

    private enum CONF_ID {
        ID_CLIENT,
        IP,
        PORT,
        PORT_EMAIL
    }

    protected long idClient=1;
    protected String IP="127.0.0.1";
    protected long port=6363;
    protected long portEmail=7373;

    protected MaLog l;
    {
        l = new MaLog(getClass());
    }

    public MaLog getL() {
        return l;
    }
    
    final static private String DEFAULT_CLIENT_FILE_NAME="client.conf";

    public String getDEFAULT_CLIENT_FILE_CONFIG() {
        return main.MaMachine.CONF_PATH+DEFAULT_CLIENT_FILE_NAME;
    }
    
    public ClientConf() {
        this(DEFAULT_CLIENT_FILE_NAME);
    }

    public ClientConf(String fileConf) {
        fileConf = main.MaMachine.CONF_PATH + fileConf;
        File f = new File(fileConf);
        JSONMa obj = new JSONMa();
        if (!f.exists()) {
            new File(main.MaMachine.CONF_PATH).mkdirs();
            obj.put(CONF_ID.ID_CLIENT.name(), idClient);
            obj.put(CONF_ID.IP.name(), IP);
            obj.put(CONF_ID.PORT.name(), port);
            obj.put(CONF_ID.PORT_EMAIL.name(), portEmail);
            try {
                obj.writeFile(fileConf);
            } catch (IOException ex) {
                Logger.getLogger(ClientConf.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(-100);
            }
        }
        try {
            obj.readFile(fileConf);
            if (MaLog.DBG) l.verbose(obj.pretty());
            idClient = (Long) obj.get(CONF_ID.ID_CLIENT.name());
            IP = (String) obj.get(CONF_ID.IP.name());
            port = (Long) obj.get(CONF_ID.PORT.name());
            portEmail = (Long) obj.get(CONF_ID.PORT_EMAIL.name());
            return;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClientConf.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClientConf.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (MaLog.DBG) l.wtf("Problemi file Client di configurazione!");
        System.exit(-1);
    }

    static public void main(String[] args) {
        ClientConf clientConf = new ClientConf();
    }
}
