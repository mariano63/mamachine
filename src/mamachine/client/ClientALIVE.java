/*
 */
package mamachine.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.CMD_ON_SOCKET;
import utility.MaLog;

/**
 *
 * @author maria
 */
public class ClientALIVE extends ClientConf{

    /**
     * dati classe
     */
    private ExecutorService executor = Executors.newSingleThreadExecutor();
     
    /**
     * Classe Thread client Loop infinito, se il Server non risponde attende
     * qualche secondo e riprova Se il Server spedisce un comando con un id
     * diverso dal client viene generato un messaggio su coda di CLIENT_STATUS
     */
    private class ThClientALIVE implements Runnable {

        private final int SECONDI_ATTESA_TRA_2_LISTENING = 5;//Secondi riconnessione

        public ThClientALIVE() {
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try (Socket cs = new Socket(IP, Integer.parseInt(portEmail+"")); DataInputStream dis = new DataInputStream(cs.getInputStream())) {
                    //write to Server
                    try (DataOutputStream dos = new DataOutputStream(cs.getOutputStream())) {
                        CMD_ON_SOCKET cos = CMD_ON_SOCKET.CLIENT_ALIVE.setId(idClient).setPar("I'm Alive!");
                        dos.writeUTF(cos.toString());
                        if (MaLog.DBG) l.data("Send to Server:"+cos.toString());
                    }
                } catch (IOException ex) {
                    if (ex instanceof ConnectException) {
                        if (MaLog.DBG) l.info("ServerALIVE not found!");
                    } else {
                        Logger.getLogger(ClientALIVE.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                try {
                    Thread.sleep(SECONDI_ATTESA_TRA_2_LISTENING * 1000);
                } catch (InterruptedException ex) {
                    if (MaLog.DBG) l.info("ClientALIVE interrupted!");
                    return;
                }
            }
            if (MaLog.DBG) l.info("Thread ClientALIVE[] terminated!");
        }
    }

    public ClientALIVE begin() {
        executor = Executors.newSingleThreadExecutor();
        executor.execute(new ThClientALIVE());
        return this;
    }

    public ClientALIVE end() {
        if (MaLog.DBG) l.verbose("Performing ClientALIVE shutdown cleanup...");
        executor.shutdown();
        try {
            if (!executor.awaitTermination(3, TimeUnit.SECONDS)) {
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            executor.shutdownNow();
        }
        if (MaLog.DBG) l.verbose("Done cleaning");
        return this;
    }

    public boolean isTerminated() {
        return executor.isTerminated() | executor.isShutdown();
    }
    
    public ClientALIVE() {
        super();
    }
    
    static public void main(String[] args)  {
        ClientALIVE c = new ClientALIVE().begin();
        while (!c.isTerminated()) {
            try {
                Thread.sleep(30*1000);
            c.end();
            } catch (InterruptedException ex) {
                Logger.getLogger(ClientALIVE.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
