/*
 */
package mamachine.client;

import JSONMa.JSONMa;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import utility.MaLog;
import main.CMD_ON_SOCKET;
import mamachine.server.ServerMsg.SERVER_KEYS;

/**
 *
 * @author maria
 */
public class ClientMsg extends ClientConf{

    /**
     * variabili e dati
     */

    protected final BlockingQueue<String> coda = new ArrayBlockingQueue(1);

    static public enum CLIENT_KEYS {
        CMD, PARAM, ID_CLIENT, INSTANT
    }

    /**
     * Ricevuta una stringa da server. (CMD_ON_SOCKET)
     * che verrà inserito nella coda e spedito poi al Server. Questo metodo si
     * trova esattamente in mezzo alla comunicazione Server-Client
     * Server(receive) -X- Client(send). No comandi multipli.
     * Se l' ID_SERVITO differisce dall' ID_CLIENT viene generato un CLIENT_NACK
     * che non incrementa il progCounter del server, che quindi rimane sul comando attuale
     * rispedendolo fino ad individuare il Client che intende servire.
     * (L' incremento progCounter, si trova in ServerMsg() ).
     *
     * @param sServerMsg
     */
    public void ricezioneDaServer(String sServerMsg) {
        JSONMa jsonClient = new JSONMa().parse(sServerMsg);
        /**
         * qui si leggono i valori dal messaggio Server
         */
        long idServito = (Long) jsonClient.get(SERVER_KEYS.ID_SERVITO.name());
        String cmd = (String) jsonClient.get(SERVER_KEYS.CMD.name());
        String param = (String) jsonClient.get(SERVER_KEYS.PARAM.name());
        String instant = (String) jsonClient.get(SERVER_KEYS.INSTANT.name());
        if (MaLog.DBG) l.data(String.format("[From Server]:%s", jsonClient.pretty()));
        /**
         * Qui si gestisce il messaggio da spedire al server
         */
        CMD_ON_SOCKET commands = CMD_ON_SOCKET.valueOf(cmd);
        //un SERVER_STATUS implica sempre una risposta da parte del Client con un
        //CLIENT_STATUS, anche se non matcha ID.
        //Serve per sapere se il Client è vivo.
//        if (commands == CMD_ON_SOCKET.SERVER_STATUS) {
//            coda.add(CMD_ON_SOCKET.CLIENT_STATUS.setPar("@").toString());
//            return;
//        }
        if (idServito != idClient) {
            coda.add(CMD_ON_SOCKET.CLIENT_NACK.setPar("**ERR: ID non coincide.").toString());
            return;
        }
        switch (CMD_ON_SOCKET.valueOf(cmd)) {
            case SERVER_IDLE:
                coda.add(CMD_ON_SOCKET.CLIENT_IDLE.setPar("@").setId(idClient).toString());
                return;
            case SERVER_EXEC:   //parte il game
                coda.add(CMD_ON_SOCKET.CLIENT_EXEC.setPar("@").setId(idClient).toString());
                /**
                 * Partenza .jar | .exe
                 */
                 new utility.ExecExtern("java -jar", main.MaMachine.CONF_PATH, param, "").go();
                return;
        }
        coda.add(CMD_ON_SOCKET.CLIENT_STATUS.setPar("@").setId(idClient).toString());
    }
    /**
     *
     */
    public ClientMsg() {
        super();
    }

}
