/*
 */
package mamachine.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import utility.MaLog;

/**
 *Client. Gira in un suo Thread in loop infinito, 
 * se non trova un server attende X secondi e riprova
 * @author maria
 */
public class Client extends ClientMsg{

    /**
     * dati classe
     */
    
    /**
     * Thread executor ove girerà il client
     */
    private ExecutorService executor = Executors.newSingleThreadExecutor();
     
    /**
     * Classe Thread client Loop infinito, se il Server non risponde attende
     * qualche secondo e riprova Se il Server spedisce un comando con un id
     * diverso dal client viene generato un messaggio su coda di 
     * CLIENT_NACK, 
     * @see mamachine.client.ClientMsg ricezioneDaServer()
     */
    private class ThClient implements Runnable {

        private final int SECONDI_ATTESA_TRA_2_LISTENING = 3;//Secondi riconnessione

        public ThClient() {
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try (Socket cs = new Socket(IP, Integer.parseInt(port+"")); DataInputStream dis = new DataInputStream(cs.getInputStream())) {
                    //read from Server a CMD_ON_SOCKET as String Object
                    ricezioneDaServer(dis.readUTF());
                    //write to Server a CMD_ON_SOCKET as String Object 
                    try (DataOutputStream dos = new DataOutputStream(cs.getOutputStream())) {
                        dos.writeUTF(coda.poll());
                    }
                } catch (IOException ex) {
                    if (ex instanceof ConnectException) {
                        if (MaLog.DBG) l.info("Server not found!");
                    } else {
                        Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                try {
                    Thread.sleep(SECONDI_ATTESA_TRA_2_LISTENING * 1000);
                } catch (InterruptedException ex) {
                    if (MaLog.DBG) l.info("Client interrupted!");
                    return;
                }
            }
            if (MaLog.DBG) l.info("Thread Client[] terminated!");
        }
    }

    public Client begin() {
        executor = Executors.newSingleThreadExecutor();
        executor.execute(new ThClient());
        return this;
    }

    public Client end() {
        if (MaLog.DBG) l.verbose("Performing some shutdown cleanup...");
        executor.shutdown();
        try {
            if (!executor.awaitTermination(3, TimeUnit.SECONDS)) {
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            executor.shutdownNow();
        }
        if (MaLog.DBG) l.verbose("Done cleaning");
        return this;
    }

    public boolean isTerminated() {
        return executor.isTerminated() | executor.isShutdown();
    }
    
    public Client() {
        super();
    }
    
    static public void main(String[] args)  {
        Client c = new Client().begin();
        while (!c.isTerminated()) {
            try {
                Thread.sleep(15000);
//            Client.getInstance().end();
            } catch (InterruptedException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
