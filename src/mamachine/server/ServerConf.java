/*
 */
package mamachine.server;

import JSONMa.JSONMa;
import mamachine.client.ClientConf;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utility.MaLog;

/**
 *
 * @author maria
 */
public class ServerConf {
    //dati Email
    protected String emailUsername="artlab.electronic";
    protected String emailPassword="Che Password Uso";
    protected String emailSubject="Report of Client";
    protected String emailMessage="Report of Client...";
    protected String emailaddTo="mmarry57@yahoo.it,mariano.g@outlook.it";
    protected String emailFrom="Server.ciccio@me.com";
    protected String emailHostname="smtp.googlemail.com";
    protected String emailLogoURL="email_pic.jpg";
    protected String emailHtmlMsg= "<html><H2>Problemi su Client:%d</H2><br><br> - ";
    //altri dati
    protected long port=6363;
    protected long portEmail=7373;
    protected List<Object> lstClients = new ArrayList<>();
    {Collections.addAll(lstClients, 1, 2);}
    
    protected MaLog l;
    {
        l = new MaLog(getClass());
    }

    public MaLog getL() {
        return l;
    }
    
    final static public String DEFAULT_SERVER_FILE_NAME="server.conf";

    public String getDEFAULT_SERVER_FILE_CONFIG() {
        return main.MaMachine.CONF_PATH+DEFAULT_SERVER_FILE_NAME;
    }

    private enum CONF_ID {
        IDS_LIST,
        PORT,
        PORT_EMAIL,
        USERNAME,
        PASSWORD,
        SUBJECT,
        MESSAGE,
        ADDTO,
        FROM,
        HOSTNAME,
        LOGOURL,
        HTMLMSG
    }

    public ServerConf() {
        this(DEFAULT_SERVER_FILE_NAME);
    }

    public ServerConf(String fileConf) {
        fileConf = main.MaMachine.CONF_PATH + fileConf;
        File f = new File(fileConf);
        JSONMa obj = new JSONMa();
        if (!f.exists()) {
            new File(main.MaMachine.CONF_PATH).mkdirs();
            obj.put(CONF_ID.PORT.name(), port);
            obj.put(CONF_ID.PORT_EMAIL.name(), portEmail);
            obj.put(CONF_ID.IDS_LIST.name(), lstClients);
            obj.put(CONF_ID.USERNAME.name(), emailUsername);
            obj.put(CONF_ID.PASSWORD.name(), emailPassword);
            obj.put(CONF_ID.SUBJECT.name(), emailSubject);
            obj.put(CONF_ID.MESSAGE.name(), emailMessage);
            obj.put(CONF_ID.HOSTNAME.name(), emailHostname);
            obj.put(CONF_ID.ADDTO.name(), emailaddTo);
            obj.put(CONF_ID.FROM.name(), emailFrom);
            obj.put(CONF_ID.HTMLMSG.name(), emailHtmlMsg);
            obj.put(CONF_ID.LOGOURL.name(), emailLogoURL);
            try {
                obj.writeFile(fileConf);
            } catch (IOException ex) {
                Logger.getLogger(ServerConf.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(-101);
            }
        }

        try {
            obj.readFile(fileConf);
            if (MaLog.DBG) l.verbose(obj.pretty());
            port = (Long) obj.get(CONF_ID.PORT.name());
            portEmail = (Long) obj.get(CONF_ID.PORT_EMAIL.name());
            lstClients = Arrays.asList((Object[])obj.get(CONF_ID.IDS_LIST.name()));
            emailUsername=(String) obj.get(CONF_ID.USERNAME.name());
            emailPassword=(String) obj.get(CONF_ID.PASSWORD.name());
            emailSubject=(String) obj.get(CONF_ID.SUBJECT.name());
            emailMessage=(String) obj.get(CONF_ID.MESSAGE.name());
            emailaddTo=(String) obj.get(CONF_ID.ADDTO.name());
            emailFrom=(String) obj.get(CONF_ID.FROM.name());
            emailHostname=(String) obj.get(CONF_ID.HOSTNAME.name());
            emailLogoURL=(String) obj.get(CONF_ID.LOGOURL.name());
            emailHtmlMsg= (String) obj.get(CONF_ID.HTMLMSG.name());
            return;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClientConf.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClientConf.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (MaLog.DBG) l.wtf("Problemi file Server di configurazione!");
        System.exit(-1);
    }

    static public void main(String[] args) {
        ServerConf serverConf = new ServerConf();
    }
}
