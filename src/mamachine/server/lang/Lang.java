/*Language commands
 */
package mamachine.server.lang;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import main.CMD_ON_SOCKET;
import utility.MaLog;
import mamachine.server.ServerMsg;

/**
 *
 * @author maria
 */
public class Lang extends ServerMsg {

    private MaLog langLog = new MaLog(getClass());

    public enum CMD {
        VAR("1,1"),
        ADD("1,1"),
        ADDSTR("1,1"),
        LABEL("lab"),
        GOTO("lab"),
        WAIT("1"),
        SERVER_CMD("from enum COMMANDS");
        String par;

        @Override
        public String toString() {
            return this.name() + "," + par;
        }

        private CMD(String par) {
            this.par = par;
        }

        public CMD setPar(String par) {
            this.par = par;
            return this;
        }

        public String getPar() {
            return par;
        }

    }
    protected final String DEFAULT_LANGUAGE_FILE = main.MaMachine.CONF_PATH + "command.lang";

    public String getDEFAULT_LANGUAGE_FILE() {
        return DEFAULT_LANGUAGE_FILE;
    }
    /**
     * Mappa contenente variabili (int o String)
     */
    private final Map<String, Object> variabili = new HashMap<>();

    /**
     * Map contenente labels
     */
    private final Map<String, Integer> labels = new HashMap();

    /**
     *
     */
    public Lang() {
        super();
        //Se il file non esiste verranno scritte le linee seguenti
        lstCmd.add(CMD.LABEL);
        lstCmd.add(CMD.WAIT.setPar("5"));
        lstCmd.add(CMD_ON_SOCKET.SERVER_STATUS.setPar("Listening clients:" + lstClients.toString().replace(",", ";")).
                setId(1));
        lstCmd.add(CMD_ON_SOCKET.SERVER_EXEC.setPar("JFXAstroG1/dist/JFXAstroG1.jar").setId(1));
        lstCmd.add(CMD.GOTO);
        //Legge il file se esiste, altrimenti lo crea
        readFileCmd();
    }

    /**
     * esegue il comando
     *
     * @param cmd
     */
    public void executeLang(CMD cm) {

        /**
         * se l' Object è un CMD lo esegue ma non lo manda in coda
         */
        CMD cmd = (CMD) cm;
        String par[] = cmd.getPar().split(",");
        langLog.verbose("[Lang]" + cmd.toString());
        switch (cmd) {
            case VAR:
                variabili.put(par[0], (par[1].contains("\""))
                        ? par[1].replace("\"", "") : Long.parseLong(par[1]));
                break;
            case ADD:
                Object s = variabili.get(par[0]);
                if (s == null) {
                    variabili.put(par[0], (par[1].contains("\""))
                            ? par[1].replace("\"", "") : Long.parseLong(par[1]));
                } else {
                    variabili.put(par[0], (s instanceof String)
                            ? (String) s + par[1] : (Long) s + Long.parseLong(par[1]));
                }
            case LABEL:
                labels.put(par[0], getProgCounter());
                break;
            case GOTO:
                setProgCounter(labels.get(par[0]));
                break;
            case WAIT: {
                try {
                    Thread.currentThread().sleep(Integer.parseInt(par[0]) * 1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Lang.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        incProgCounter();
    }

    /**
     * Legge nella lista comandi e muove il comando(CMD o CMD_ON_SOCKET(in
     * formato String)) in coda
     *
     * @return
     */
    protected Object readOneCmd() {
        Object o = lstCmd.get(progCounter);
        return (o instanceof CMD) ? o : ((String) ((CMD_ON_SOCKET) o).toString());
    }

    /**
     * Legge la String e la trasforma in CMD o CMD_ON_SOCKET
     *
     * @param s
     * @return
     */
    private static Object toCMDOrCMD_ON_SOCKET(String s) {
        int ndx = s.indexOf(",");
        try {
            CMD c = CMD.valueOf(s.substring(0, ndx).toUpperCase());
            c.setPar(s.substring(ndx + 1));
            return c;
        } catch (IllegalArgumentException ex) {
            CMD_ON_SOCKET c = CMD_ON_SOCKET.valueOf(s.substring(0, ndx).toUpperCase());
            /**
             * Qui prendo l' idServito
             */
            String[] sTmp = s.replace(" ", "").split(",");
            if (sTmp.length != 3) {
                System.out.println("ERRORE File language, [CMD_ON_SOCKET] syntax error!(missing id_servito?)");
                System.out.println("-->"+s);
                System.exit(-101);
            }
            c.setId(Long.parseLong(sTmp[2]));
            /**
             * resto dei parametri
             */
            c.setPar(sTmp[1]);
            return c;
        }
    }

    /**
     * Scrive il file command.lang
     */
    private void writeFileCmd() {
        new File(main.MaMachine.CONF_PATH).mkdirs();
        try {
            try (FileWriter writer = new FileWriter(DEFAULT_LANGUAGE_FILE)) {
                for (Object c : lstCmd) {
                    if (c instanceof CMD) {
                        writer.write(((CMD) c).toString() + System.getProperty("line.separator"));
                    } else if (c instanceof CMD_ON_SOCKET) {
                        writer.write(((CMD_ON_SOCKET) c).toStringRaw() + System.getProperty("line.separator"));
                    }
                }
            }
        } catch (IOException ex1) {
            Logger.getLogger(Lang.class.getName()).log(Level.SEVERE, null, ex1);
        }
    }

    /**
     * Legge il file command.lang, se non esiste lo crea con dati inseriti nel
     * costruttore
     */
    private void readFileCmd() {
        try (Stream<String> stream = Files.lines(Paths.get(DEFAULT_LANGUAGE_FILE))) {
            lstCmd.clear();
            stream.forEach(s -> {
                langLog.verbose("Load Lang file line:" + s);
                lstCmd.add(toCMDOrCMD_ON_SOCKET(s));
            });

        } catch (IOException ex) {
//            Logger.getLogger(Lang.class.getName()).log(Level.SEVERE, null, ex);
            langLog.verbose("<command.lang non esiste, lo creo!>");
            writeFileCmd();
        }
    }

    /**
     * test
     *
     * @param args
     */
    public static void main(String[] args) {
        Lang lang = new Lang();
        System.out.println(lang.getLstCmd().toString());
    }
}
