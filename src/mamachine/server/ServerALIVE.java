/*
 */
package mamachine.server;

import JSONMa.JSONMa;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import utility.MaLog;
import mamachine.client.ClientMsg;

/**
 *Server ALIVE.
 * In attesa di comandi ALIVE provenienti dai clients collegati.
 * @author maria
 */
public class ServerALIVE extends ServerConf {
    /**
     * Se non arriva un messaggio client entro SECS_BETWEEN_INSTANT secondi si manda email
     */
    public final int SECS_BETWEEN_INSTANT = 10*1000; 
    /**
     * Una volta partito il server si attendono SECS_TIMER_TOSTART secondi e si fa partire 
     * la schedulazione del timer ad intervalli di SECS_TIMER_SCHEDULE secondi.
     */
    public final int SECS_TIMER_TOSTART = 10*1000; 
    /**
     * Intervallo di schedulazione per il timer check email
     */
    public final int SECS_TIMER_SCHEDULE = 10*1000; 
    TimerTask timerTask;
    protected HashMap<String,Instant> mapToCheckEmail = new HashMap();
    private ServerSocket ss;
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    class thServer implements Runnable {

        public thServer() throws IOException {
            ss = new ServerSocket(Integer.parseInt(portEmail + ""));
        }

        @Override
        public void run() {
            if (MaLog.DBG) l.verbose("Server start...");
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    if (MaLog.DBG) l.verbose("Wait client ALIVE...");
                    ss.setSoTimeout(15000);
                    Socket socket = ss.accept();
                        try (DataInputStream dis = new DataInputStream(socket.getInputStream())) {
                            String s = (dis.readUTF());
                            JSONMa jsm = new JSONMa().parse(s);
                            mapToCheckEmail.put((Long)jsm.get(ClientMsg.CLIENT_KEYS.ID_CLIENT.name())+"",
                                    Instant.parse((String)jsm.get(ClientMsg.CLIENT_KEYS.INSTANT.name())));
                        }
                } catch (IOException ex) {
                    if (ex instanceof SocketTimeoutException) {
                        if (MaLog.DBG) l.info("ServerALIVE accept() timeout!");
                    } else {
                        Logger.getLogger(ServerALIVE.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            if (MaLog.DBG) l.info("ServerALIVE interrupted!");
            try {
                ss.close();
            } catch (IOException ex) {
                Logger.getLogger(ServerALIVE.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ServerALIVE begin() {
        executor = Executors.newSingleThreadExecutor();
        try {
            lstClients.forEach(v->mapToCheckEmail.put(v+"", Instant.now()));
            executor.execute(new thServer());
            timerTask = new ChEmail();
            Timer timer = new Timer(true);
            timer.scheduleAtFixedRate(timerTask, SECS_TIMER_TOSTART, SECS_TIMER_SCHEDULE);
        } catch (IOException ex) {
            if (MaLog.DBG) l.error("ServerALIVE impossibilitato allo start!\n" + ex.getMessage());
        }
        return this;
    }

    public ServerALIVE end() {
        if(timerTask!=null)
            timerTask.cancel();
        if (MaLog.DBG) l.info("Performing ServerALIVE shutdown cleanup...Timer canceled.");
        executor.shutdown();
        try {
            if (!executor.awaitTermination(3, TimeUnit.SECONDS)) {
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            executor.shutdownNow();
        }
        if (MaLog.DBG) l.info("Done cleaning");
        return this;
    }

    public boolean isTerminated() {
        return executor.isTerminated() | executor.isShutdown();
    }

    public ServerALIVE() {
        super();
        if (MaLog.DBG) l.info(String.format(
                "Create ServerALIVE on port:%d, listening Clients:%s", portEmail, lstClients.toString()));
    }

    private class ChEmail extends TimerTask{

        @Override
        public void run() {
//            mapToCheckEmail.forEach((k,v)->System.out.println(k+" - "+ v+" -"+mapToCheckEmail.size()));
            mapToCheckEmail.forEach((k,v)->{
                Duration timeElapsed = Duration.between(v, Instant.now());
                if(timeElapsed.toMillis()>SECS_BETWEEN_INSTANT){
                    System.err.println("SEND EMAIL OF CLIENT "+k);
                }
            }
            );
        }
        
    }
    public static void main(String[] args) {
        ServerALIVE s = new ServerALIVE().begin();
        while (!s.isTerminated()) {
            try {
                Thread.sleep(30*1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ServerALIVE.class.getName()).log(Level.SEVERE, null, ex);
            }
            s.end();
        }
    }
}
