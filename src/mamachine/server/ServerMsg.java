/*
 */
package mamachine.server;

import JSONMa.JSONMa;
import mamachine.client.ClientMsg;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import utility.MaLog;
import main.CMD_ON_SOCKET;

/**
 *
 * @author maria
 */
public class ServerMsg extends ServerConf{
//public class ServerMsg extends CheckEmail{

    protected final BlockingQueue<Object> coda = new ArrayBlockingQueue(1);
    
    public static enum SERVER_KEYS {
        CMD, PARAM, ID_SERVITO,INSTANT
    }
    /** **idServito
     * E' l' ID del client attualmente servito nei messaggi spediti verso qualsiasi client
     * provi a colloquiare col server.
     * Serve per filtrare univocamente il comando verso uno ed un solo client.
     */
     protected long idServito;
     
     /**
      * la lista può contenere CMD_ON_SOCKET o CMD, e viene letta da file 
      * E' l' insieme dei comandi che il server effettuerà in sequenza.
      */
    protected ArrayList<Object> lstCmd = new ArrayList<>();

    /**
     * 
     * @return 
     */
    public ArrayList<Object> getLstCmd() {
        return lstCmd;
    }
    protected int progCounter = 0;
    /**
     * Ricevuta una stringa da Client (CMD_ON_SOCKET)
     * Questo metodo compilerà un altro CMD_ON_SOCKET.
     * che verrà inserito nella coda e spedito poi al Client. Questo metodo si
     * trova alla fine della comunicazione. Server(Send) - Client(receive) -X-.
     * E il comando in coda verrà quindi spedito alla prossima richiesta di
     * socket da parte del client. I comandi aggiunti alla coda possono essere multipli.
     *
     * @param sClientString
     */
    protected void ricezioneDaClient(String sClientString) {
        JSONMa jsonObject = new JSONMa().parse(sClientString);
        if (MaLog.DBG) l.data(String.format("[From Client]:%s",(jsonObject.pretty())));
        /**
         * qui si leggono i valori dal messaggio Client
         */         
        long idFromClient = (Long) jsonObject.get(ClientMsg.CLIENT_KEYS.ID_CLIENT.name());
        String param = (String) jsonObject.get(ClientMsg.CLIENT_KEYS.PARAM.name());
        String cmd = (String) jsonObject.get(ClientMsg.CLIENT_KEYS.CMD.name());
        String instant = (String) jsonObject.get(ClientMsg.CLIENT_KEYS.INSTANT.name());
        //!!rimetti nel caso ServerMsg extends CheckEmail 
        //setMapChekEmail(idFromClient+"", Instant.parse(instant));
        /**
         * In base al COMMANDS del ClientMsg compila campi del ServerMsg, ed esegue vari task.
         */
        CMD_ON_SOCKET commands=CMD_ON_SOCKET.valueOf(cmd);
        switch (commands) {
            case CLIENT_NACK:   //Nessun incremento progCounter...
                return;
            case CLIENT_IDLE:
            case CLIENT_STATUS:
            case CLIENT_EXEC://il Client ha ricevuto ordine esecuzione
            default:
        }
//        coda.add(CMD_ON_SOCKET.SERVER_STATUS.setPar("Listenings Clients:"+lstClients).toString());
        incProgCounter();
    }

    /**
     * Incremento del Program Counter
     * Se raggiunta fine, riparte dall' inizio
     */
    protected void incProgCounter() {
        if (++progCounter > lstCmd.size()) {
            progCounter = 0;
        }
    }

    public int getProgCounter() {
        return progCounter;
    }

    public void setProgCounter(int progCounter) {
        this.progCounter = progCounter;
    }
    
    /**
     * 
     * @return 
     */
    public BlockingQueue<Object> getCoda() {
        return coda;
    }
    /**
     *
     */
    public ServerMsg() {
        super();
    }

}
