/**
 * Gestione email ed Id Servito del server.
 * **MAIL
 * Se un Client presente nella lista ServerConf.lstClients, non contatta
 * il server dopo almeno 15 secondi, questa classe si incarica di spedire una email
 * con valori di configurazione presenti nel ServerConf.

 */
package mamachine.server;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author maria
 */
public class CheckEmail extends ServerConf {
    /**
     * Se non arriva un messaggio client entro SECS_BETWEEN_INSTANT secondi si manda email
     */
    public final int SECS_BETWEEN_INSTANT = 10*1000; 
    /**
     * Una volta partito il server si attendono SECS_TIMER_TOSTART secondi e si fa partire 
     * la schedulazione del timer ad intervalli di SECS_TIMER_SCHEDULE secondi.
     */
    public final int SECS_TIMER_TOSTART = 100*1000; 
    /**
     * Intervallo di schedulazione per il timer check email
     */
    public final int SECS_TIMER_SCHEDULE = 10*1000; 
    
    protected HashMap<String,Instant> mapToCheckEmail = new HashMap();
    
    
    public CheckEmail() {
        super();
        lstClients.forEach(v->mapToCheckEmail.put(v+"", Instant.now()));
        
        TimerTask timerTask = new ChEmail();
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(timerTask, SECS_TIMER_TOSTART, SECS_TIMER_SCHEDULE);
    }

    /**
     * set all for normal use and start to work on emails
     */
    protected final void startEmailCheck(){
    }

    protected void setMapChekEmail(String keyId, Instant valueInstant) {
        mapToCheckEmail.put(keyId, valueInstant);
    }

    private class ChEmail extends TimerTask{

        @Override
        public void run() {
//            mapToCheckEmail.forEach((k,v)->System.out.println(k+" - "+ v+" -"+mapToCheckEmail.size()));
            mapToCheckEmail.forEach((k,v)->{
                Duration timeElapsed = Duration.between(v, Instant.now());
                if(timeElapsed.toMillis()>SECS_BETWEEN_INSTANT){
                    System.err.println("SEND EMAIL OF CLIENT "+k);
                }
            }
            );
        }
        
    }
    
}
