/*
 */
package mamachine.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import utility.MaLog;
import mamachine.server.lang.Lang;

/**
 *
 * @author maria
 */
public class Server extends Lang {

    private ServerSocket ss;
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    class thServer implements Runnable {

        public thServer() throws IOException {
            ss = new ServerSocket(Integer.parseInt(port + ""));
        }

        @Override
        public void run() {
            if (MaLog.DBG) l.verbose("Server start...");
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    if (MaLog.DBG) l.verbose("Wait client...");
                    ss.setSoTimeout(15000);
                    Socket socket = ss.accept();
                    try (DataOutputStream dos = new DataOutputStream(socket.getOutputStream())) {
                        /**
                         * write to Client - importante.
                         * i comandi CLIENT - SERVER viaggiano come oggetti String
                         * i comandi del linguaggio vanno in coda come object lang.CMD in loop
                         */
                        Object s;
                        for (;;) {
                            s = coda.poll();
                            if (s==null) s = readOneCmd();
                            //se c'è un comando da Lang
                            if (s instanceof CMD) executeLang((CMD)s);
                            else break;
                        }
                        dos.writeUTF((String)s);
                        //read from Client and manage the queue
                        try (DataInputStream dis = new DataInputStream(socket.getInputStream())) {
                            ricezioneDaClient(dis.readUTF());
                        }
                    }
                } catch (IOException ex) {
                    if (ex instanceof SocketTimeoutException) {
                        if (MaLog.DBG) l.verbose("Server accept() timeout!");
                    } else {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            if (MaLog.DBG) l.info("Server interrupted!");
            try {
                ss.close();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public Server begin() {
        executor = Executors.newSingleThreadExecutor();
        try {
            executor.execute(new thServer());
        } catch (IOException ex) {
            if (MaLog.DBG) l.wtf("Server impossibilitato allo start!\n" + ex.getMessage());
        }
        return this;
    }

    public Server end() {
        if (MaLog.DBG) l.verbose("Performing Server shutdown cleanup...");
        executor.shutdown();
        try {
            if (!executor.awaitTermination(3, TimeUnit.SECONDS)) {
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            executor.shutdownNow();
        }
        if (MaLog.DBG) l.verbose("Done cleaning");
        return this;
    }

    public boolean isTerminated() {
        return executor.isTerminated() | executor.isShutdown();
    }

    public long getPort() {
        return port;
    }

    public Server() {
        super();
        if (MaLog.DBG) l.info(String.format(
                "Create Server on port:%d, listening Clients:%s", port, lstClients.toString()));
    }

    public static void main(String[] args) {
        Server s = new Server().begin();
        while (!s.isTerminated()) {
            try {
                Thread.sleep(15000);
//            Server.getInstance().end();
            } catch (InterruptedException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
