/*
 */
package mamachine.server;


/**
 *
 * @author maria
 */
    
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;

/**
 *
 * @author maria
 */
public final class SendEmail extends ServerConf{
  
    private final long clientId;

    /**
     * @param clientId 
     */
    public SendEmail(long clientId) {
        super();
        this.clientId=clientId;
    }
    
    /**
     * Send Email in html format.
     * Se presente Client logo.(jpg|png) viene visualizzato il file, altrimenti viene 
     * spedito il testo alternativo.
     */
    public void sendHtml() {
        HtmlEmail email = new HtmlEmail();
        email.setHostName(emailHostname);
        email.setSubject(emailSubject);
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(emailUsername, emailPassword));
        email.setSSLOnConnect(true);

        try {
            email.setFrom(emailFrom);
            //AddTo
            String[] st = emailaddTo.replace(" ", "").split(",");
            for (String s : st) {
                email.addTo(s);
            }
            // embed the image and get the content id
//            URL url = getClass().getResource(logoURL).toURI().toURL();
            URL url;
            try {
                //Se non esiste il logo, manda il testo alternativo
                File file = new File(main.MaMachine.CONF_PATH + emailLogoURL);
                if(file.exists()){
                    url = file.toURI().toURL();
                   String cid = email.embed(url, "Client logo");
                   // set the html message
                   email.setHtmlMsg(String.format(emailHtmlMsg+"<img src=cid:%s></html>", clientId, cid));  
                }
            } catch (MalformedURLException ex) {
                Logger.getLogger(SendEmail.class.getName()).log(Level.SEVERE, null, ex);
            }
            // set the alternative message
            email.setTextMsg(emailMessage + " - [" + clientId + "]");
            email.send();
        } catch (EmailException ex) {
            Logger.getLogger(SendEmail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *Manda una email semplice.
     */
    public void sendSimple() {
        try {
            Email email = new SimpleEmail();
            email.setHostName(emailHostname);
            email.setSmtpPort(465);
            email.setAuthenticator(new DefaultAuthenticator(emailUsername, emailPassword));
            email.setSSLOnConnect(true);
            email.setFrom(emailFrom);
            email.setSubject(emailSubject);
            email.setMsg(emailMessage);
            //AddTo
            String[] st = emailaddTo.replace(" ", "").split(",");
            for (String s : st) {
                email.addTo(s);
            }
            //Send
            email.send();
        } catch (EmailException ex) {
            Logger.getLogger(SendEmail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * test
     * @param args
     */
    public static void main(String[] args) {
//        new SendEmail().sendSimple();
        new SendEmail(101).sendHtml();
    }
}
