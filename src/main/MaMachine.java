/*
 */
package main;

import mamachine.server.Server;
import mamachine.client.Client;
import java.io.IOException;

/**
 *
 * @author maria
 */
public class MaMachine {

    final static public String CONF_PATH
            = System.getProperty("user.home")
            + System.getProperty("file.separator")
            + "MaMachine"
            + System.getProperty("file.separator");

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        new Server().begin();
        new Client().begin();
//        new ServerALIVE().begin();
//        new ClientALIVE().begin();
    }
    
}
