/**
 * Comandi scambiati tra SERVER & CLIENT
 */
package main;

import JSONMa.JSONMa;
import java.time.Instant;
import mamachine.client.ClientMsg;
import mamachine.server.ServerMsg;

/**
 * l' enum viene messo sulla coda, mentre sul socket viaggia come oggetto
 * String. Usare .toString().
 *
 * @author maria
 */
public enum CMD_ON_SOCKET {
    //SERVER CMD_ON_SOCKET

    /**
     * Non so se utile, per ora considero di spedirlo in caso di errore
     */
    SERVER_IDLE("", -1001),
    /**
     * Manda status, tipicamente i clients di cui è in ascolto
     */
    SERVER_STATUS("", -1001),
    /**
     * Richiede esecuzione di un app sul client, par deve contenere
     * subdirectory+filename il suffisso se .jar crea un comando quale java -jar
     */
    SERVER_EXEC("", -1001),
    /**
     * il Client è vivo!
     */
    CLIENT_ALIVE("", -1001), //
    /**
     * Status del Client
     */
    CLIENT_STATUS("", -1001), //

    /**
     * Non so se utile, per ora considero di spedirlo in caso di errore
     */
    CLIENT_IDLE("", -1001),
    /**
     * Risposta a SERVER_EXEC
     */
    CLIENT_EXEC("", -1001),
    /**
     * Se ID_SERVITO non matcha ID_CLIENT il client spedisce questo Il
     * progrCounter non si incrementerà, e il server spedirà ancora il comando
     */
    CLIENT_NACK("", -1001);

    private String par;
    private long id;

    public String toStringRaw() {
        return this.name() + "," + par + "," + id;
    }

    @Override
    public String toString() {
        JSONMa obj = new JSONMa();
        if (this.name().contains("SERVER_")) {
            obj.add(ServerMsg.SERVER_KEYS.CMD.name(), this.name())
                    .add(ServerMsg.SERVER_KEYS.PARAM.name(), par)
                    .add(ServerMsg.SERVER_KEYS.INSTANT.name(), Instant.now().toString())
                    .add(ServerMsg.SERVER_KEYS.ID_SERVITO.name(), id);
        } else {
            obj.add(ClientMsg.CLIENT_KEYS.CMD.name(), this.name())
                    .add(ClientMsg.CLIENT_KEYS.PARAM.name(), par)
                    .add(ClientMsg.CLIENT_KEYS.INSTANT.name(), Instant.now().toString())
                    .add(ClientMsg.CLIENT_KEYS.ID_CLIENT.name(), id);
        }
        return obj.toString();
    }

    public String help() {
        return this.name() + ",parameters,id";
    }

    private CMD_ON_SOCKET(String par, long id) {
        this.par = par;
        this.id = id;
    }

    /**
     * Set id del comando. idServito per il server, idClient per il Client
     *
     * @param id
     * @return
     */
    public CMD_ON_SOCKET setId(long id) {
        this.id = id;
        return this;
    }

    /**
     * ottiene id(per client e server)
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getPar() {
        return par;
    }

    /**
     *
     * @param par
     * @return
     */
    public CMD_ON_SOCKET setPar(String par) {
        this.par = par;
        return this;
    }
}
