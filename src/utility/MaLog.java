/*
 */
package utility;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 *
 * @author maria
 */
public class MaLog {

    /**
     * mettendo a false uno di questi la compilazione è condizionata
     */
    final public static boolean DBG=true;
    final public static boolean DBG_VERBOSE=DBG;
    final public static boolean DBG_INFO=DBG;
    final public static boolean DBG_DATA=DBG;
    final public static boolean DBG_WARNING=DBG;
    final public static boolean DBG_ERROR=DBG;
    final public static boolean DBG_WTF=DBG;
    
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static enum LEVELS {
        VERBOSE(ANSI_WHITE),
        INFO(ANSI_CYAN),
        DATA(ANSI_GREEN),
        WARNING(ANSI_YELLOW),
        ERROR(ANSI_RED),
        WTF(ANSI_RED),
        OFF(ANSI_WHITE);
        String color;

        private LEVELS(String s) {
            color = s;
        }

        public String getColor() {
            return color;
        }
    };
    private LEVELS level = LEVELS.INFO;
    Class cl;
    PrintStream printStreamOutDefault = System.out;
    PrintStream printStreamErrDefault = System.err;
    PrintStream printStreamTarget = System.out;

    public MaLog(Class c) {
        cl = c;
    }

    /**
     * to create a Printstream on JtextArea PrintStream printStream = new
     * PrintStream(new OutputStream() { // override write methods to write to
     * the JTextArea / JTextPane });
     *
     * @param ps
     */
    public void setStreamOut(PrintStream ps) {
        printStreamTarget = ps;
        System.setOut(ps);
        System.setErr(ps);
    }

    public void setStreamOutDefault() {
        printStreamTarget = printStreamOutDefault;
        System.setOut(printStreamOutDefault);
        System.setErr(printStreamErrDefault);
    }

    public MaLog wtf(String s) {
        return (DBG_WTF) ? echo(LEVELS.WTF, s) : this;
    }

    public MaLog error(String s) {
        return (DBG_ERROR) ? echo(LEVELS.ERROR, s) : this;
    }

    public MaLog warning(String s) {
        return (DBG_WARNING) ? echo(LEVELS.WARNING, s) : this;
    }

    public MaLog verbose(String s) {
        return (DBG_VERBOSE) ? echo(LEVELS.VERBOSE, s) : this;
    }

    public MaLog info(String s) {
        return (DBG_INFO) ? echo(LEVELS.INFO, s) : this;
    }

    public MaLog data(String s) {
        return (DBG_DATA) ? echo(LEVELS.DATA, s) : this;
    }

    public MaLog echo(LEVELS l, String s) {
        if (level.ordinal() > l.ordinal()) {
            return this;
        }

        DateTimeFormatter formatter
                = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
                        .withLocale(Locale.ITALY)
                        .withZone(ZoneId.systemDefault());

        String s1 = cl.getCanonicalName() + " "
                + formatter.format(Instant.now())
                + " [" + l.toString() + "]"
                + System.getProperty("line.separator")
                + ((printStreamTarget == System.out) ? l.getColor() : "") + s + System.getProperty("line.separator");

        setStreamOut(printStreamTarget);
        System.out.print(s1);
        return this;
    }

    /**
     * printStreamTarget viene scritto in modo da reindirizzare System.setOut()
     *
     * @param jta
     * @return
     */
    public MaLog setJta(JTextArea jta) {
        printStreamTarget = new PrintStream(new OutputStream() {
            StringBuilder sb = new StringBuilder();

            @Override
            public void write(int b) throws IOException {
                if (b == '\r') {
                    return;
                }
                if (b == '\n') {
                    final String text = sb.toString() + "\n";
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            jta.append(text);
                        }
                    });
                    sb.setLength(0);
                    return;
                }

                sb.append((char) b);
            }
        });
        return this;
    }

    public MaLog setLevel(LEVELS level) {
        this.level = level;
        return this;
    }
}
