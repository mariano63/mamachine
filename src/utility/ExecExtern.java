/*
 */
package utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author maria
 */
public class ExecExtern {

    String cmd;
    JTextArea outMessages = null;

    public ExecExtern(String command) {
        switch (OsUtils.getOS()) {
            case LINUX:
            case MAC:
            case SOLARIS:
                cmd = command;  
                break;
            case WINDOWS:
                cmd = "cmd /c "+command;  
                break;
            default:
                System.out.println("**Err**,  Unknown Operating system!");
                System.exit(-1001);
        }
    }

    public ExecExtern(String command, String pathFile, String fileName, String extensionWithoutComma) {
        switch (OsUtils.getOS()) {
            case LINUX:
            case MAC:
            case SOLARIS:
                cmd = String.format("%s %s%s.%s",command, pathFile, fileName, extensionWithoutComma);
                break;
            case WINDOWS:
                cmd = String.format("cmd /c %s %s%s.%s",command, pathFile, fileName, extensionWithoutComma);
                break;
            default:
                System.out.println("**Err**,  Unknown Operating system!");
                System.exit(-1001);
        }
    }

    public ExecExtern setOutMessages(JTextArea outMessages) {
        this.outMessages = outMessages;
        return this;
    }

    public void go() {
        try {
            String line;
            Process p = Runtime.getRuntime().exec(cmd);
            BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            while ((line = bri.readLine()) != null) {
                System.out.println(line);
            }
            bri.close();
            while ((line = bre.readLine()) != null) {
                System.out.println(line);
                if (outMessages != null) {
                    outMessages.setText(line);
                }
            }
            bre.close();
            p.waitFor();
            System.out.println("Done.");
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(ExecExtern.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String args[]) {
        if (OsUtils.getOS()==OsUtils.OSES.WINDOWS)
            new ExecExtern("dir").go(); 
        else if (OsUtils.getOS()==OsUtils.OSES.LINUX || OsUtils.getOS()==OsUtils.OSES.MAC)
            new ExecExtern("ls -aF").go(); 
    }
}
