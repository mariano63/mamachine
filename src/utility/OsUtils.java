/*
 */
package utility;

/**
 * Classe di gestione differenti sistemi operativi
 * @author maria
 */
public class OsUtils {

    /**
     * Enumerazione diversi sistemi operativi possibili
     */
    public static enum OSES {
        WINDOWS,
        LINUX,
        MAC,
        SOLARIS
    }
    
    /**
     * Nome del sistema operativo attuale
     * @return 
     */
    @Override
    public String toString() {
        return getOS().name();
    }

    /**
     * Ritorna enumerazione OS attuale.
     * @return 
     */
    public static OSES getOS() {
        if( System.getProperty("os.name").toLowerCase().contains("win"))
            return OSES.WINDOWS;
        else if (System.getProperty("os.name").toLowerCase().contains("nix") ||
                System.getProperty("os.name").toLowerCase().contains("nux") ||
                System.getProperty("os.name").toLowerCase().contains("aix")
                )
            return OSES.LINUX;
        else
        if( System.getProperty("os.name").toLowerCase().contains("mac"))
            return OSES.MAC;
        else
        if( System.getProperty("os.name").toLowerCase().contains("sunos"))
            return OSES.SOLARIS;
        
        return null;
    }

    /**
     * Test, print dell' OS attuale.
     * @param args 
     */
    static public void main(String args[]) {
        System.out.println(new OsUtils().getOS()+" --> is the operating system!");
    }
}
