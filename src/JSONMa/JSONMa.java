/**
 *JSNOMa - Il mio JSON.
 * Sostenzialmente è una sottoclasse di un HashMap, a cui vengono aggiunti 
 * un parse(), un toString() una scritura-lettura da file, e un po' di colore.
 */
package JSONMa;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;
import utility.MaLog;

/**
 *
 *JSNOMa - Il mio JSON.
 * Sostenzialmente è una sottoclasse di un HashMap, a cui vengono aggiunti 
 * un parse(), un toString() una scritura-lettura da file, e un po' di colore.
 * @author maria
 */
public class JSONMa extends HashMap<Object, Object> {
    
    /**
     * static variables
     */
     
    /**
     * Enum COLORE, colori ANSI standard per consolle
     */
    public static enum COLORE {
        ANSI_RESET(""),
        ANSI_BLACK("\u001B[30m"),
        ANSI_RED("\u001B[31m"),
        ANSI_GREEN("\u001B[32m"),
        ANSI_YELLOW("\u001B[33m"),
        ANSI_BLUE("\u001B[34m"),
        ANSI_PURPLE("\u001B[35m"),
        ANSI_CYAN("\u001B[36m"),
        ANSI_WHITE("\u001B[37m");
        String colore;
        
        /**
         * Ritorna Colore eventuale print su ANSI console
         * @return 
         */
        public String getColore() {
            return colore;
        }
        /**
         * Costruttore enum COLORE 
         * @param colore 
         */
        private COLORE(String colore) {
            this.colore = colore;
        }
    }
    /**
     * Carriage return - static
     */
    private final static String CR = System.getProperty("line.separator");
    
    /**
     * Tabulazione e COLOR di default
     */
    private String TAB_COLOR = "\t" + COLORE.ANSI_GREEN.getColore();

    /**
     * Aggiunge una key,value all' oggetto JSONMa. il <value> può essere Un qualulnque oggetto
     * ma il parse leggerà solamente String(se racchiuso tra "), oppure Long se numerico, oppure
     * Object[] se contenente un ArrayList(che a sua volta conterrà String e Long)
     * 
     * @param key
     * @param value
     * @return 
     */
    public JSONMa add(Object key, Object value){
        put(key, value);
        return this;
    }
    
    /**
     *Ritorna i valori nel JSONMa in pretty mode.
     * @return 
     */
    @Override
    public String toString() {
        TAB_COLOR = "\t";
//        return "{"+pretty("\t", "\t")+"}";
        return pretty();
    }
    
    /**
     * Colora la variabile TAB_COLOR
     * 
     * @see TAB_COLOR
     * @param c
     * @return 
     */
    public JSONMa setColor(COLORE c) {
        TAB_COLOR = "\t" + c.getColore();
        return this;
    }

    /**
     * Scan di una stringa in formato JSON e lettura dei valori String o Long o Object[] presenti.
     * i valori devono essere separati da un CR, quindi :
     * {
     *      "id":10
     *      "nome":"Mario"
     *      "Array":["1",2]
     * }
     * @param st
     * @return 
     */
    public JSONMa parse(String st) {
        Scanner scanner = new Scanner(st);
        clear();
        while (scanner.hasNextLine()) {
            String s1 = scanner.nextLine();
            if (s1.compareTo("{") == 0) {   //Inizio JSON
                continue;
            }
            if (s1.compareTo("}") == 0) {   //Fine JSON
                break;
            }
            String[] s2 = s1.trim().split(":",2); //linea JSON si splitta in due
            if (s2.length < 2) {
                continue;  //permette linee vuote interne all' oggetto
            }
            Object key; //La key può essere o una String o un int
            if (s2[0].substring(0, 1).compareTo("\"") == 0) {
                key = s2[0].replace("\"", "");  //key=String
            } else {
                key = Long.parseLong(s2[0]);  //Key=int
            }
            Object value;   //Value può essere Strin,int, o Object[]
            if (s2[1].substring(0, 1).compareTo("\"") == 0) {
                value = s2[1].replace("\"", "");    //value=String
            } else if (s2[1].substring(0, 1).compareTo("[") == 0) {   //Value=Object[]
                s2[1] = s2[1].replace("[", "").replace("]", "");
                String s3[] = s2[1].split(",");
                List val = new ArrayList();
                for (String ss : s3) {
                    val.add((ss.contains("\"")) ? ss.replace("\"", "").replace(" ", "")
                            : Long.parseLong(ss.replace(" ", "")));
                }
                value = val.toArray();
            } else {
                value = Long.parseLong(s2[1]);    //Value=int
            }
            put(key, value);
        }
        return this;
    }

    /**
     * Scrive JSONMa su file usando il toString()
     * @param file
     * @throws IOException 
     */
    public void writeFile(String file) throws IOException {
        Path p = Paths.get(file);
        Files.write(p, toString().getBytes());
    }
    
    /**
     * Legge un file ed esegue il parse ritornando un JSONMa
     * @param file
     * @return JSONma 
     * @throws IOException 
     */
    public JSONMa readFile(String file) throws IOException {
        return parse(new String(Files.readAllBytes(Paths.get(file))));
    }

    /**
     * Costruisce una String usando i valory interni al JSONMa
     * @return String in formato pretty
     */
    public String pretty() {
        StringBuilder s = new StringBuilder("");
        forEach((k, v) -> {
            s.append(TAB_COLOR);
            s.append((k instanceof String) ? "\"" + k + "\"" : "" + k).append(":");
            if (v instanceof Object[]) {
                StringBuilder s1 = new StringBuilder("[");
                Stream.of(((Object[]) v)).forEach(o -> {
                    s1.append((s1.length() == 1) ? "" : ",").
                            append((o instanceof String) ? "\"" + o + "\"" : o);
                });
                s.append(s1).append("]").append(CR);
            } else if (v instanceof String) {
                s.append("\"").append(v).append("\"").append(CR);
            } else {
                s.append(v).append(CR);
            }
        });
        return "{"+CR+s.toString()+CR+"}";
    }

    /**
     * toString() grezzo appartenente al parents.
     * @return 
     */
    public String toRaw() {
        return super.toString();
    }

    /**
     * usata per test
     * @param args 
     */
    public static void main(String[] args) {
        //Crea oggetto
        JSONMa js = new JSONMa();
        js.put(1, "Uno");
        js.put("2", 2);
        js.put(3, new Object[]{"1", 2, 3});
        
        System.out.println(MaLog.ANSI_PURPLE+"System.out.println(js.parse(js.toString()).toString()); = ");
        System.out.println(js.parse(js.toString()).toString());
        System.out.println("--------------------------------------");
        System.out.println(MaLog.ANSI_PURPLE+"js.pretty() = ");
        System.out.println(js.pretty());
        System.out.println("--------------------------------------");
        System.out.println(MaLog.ANSI_PURPLE+"js.toRaw() = ");
        System.out.println(js.toRaw());
    }
}
